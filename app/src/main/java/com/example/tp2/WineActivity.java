package com.example.tp2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;



public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();
        final Wine wine = intent.getParcelableExtra("wine");
        final int requestID = intent.getIntExtra("request", 0);

        final TextView name = findViewById(R.id.wineName);
        name.setText(wine.getTitle());

        final TextView region = findViewById(R.id.editWineRegion);
        region.setText(wine.getRegion());

        final TextView loca = findViewById(R.id.editLoc);
        loca.setText(wine.getLocalization());

        final TextView climat = findViewById(R.id.editClimate);
        climat.setText(wine.getClimate());

        final TextView area = findViewById(R.id.editPlantedArea);
        area.setText(wine.getPlantedArea());

        Button save_btn = findViewById(R.id.button);
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(String.valueOf(name.getText()).equals("") == true ) {
                    AlertDialog alertDialog = new AlertDialog.Builder(WineActivity.this).create();
                    alertDialog.setTitle("Sauvegarde impossible");
                    alertDialog.setMessage("Le nom du vin doit être non vide !");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CANCEL", new DialogInterface.OnClickListener() { public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }});
                    alertDialog.show();
                }else {
                    wine.setTitle(name.getText().toString());
                    wine.setRegion(region.getText().toString());
                    wine.setLocalization(loca.getText().toString());
                    wine.setClimate(climat.getText().toString());
                    wine.setPlantedArea(area.getText().toString());
                    WineDbHelper BDbHelper = new WineDbHelper(WineActivity.this);

                    switch (requestID) {
                        case 1:
                            Toast.makeText(WineActivity.this, "Le vin '" + wine.getTitle() + "' à été mis à jour", Toast.LENGTH_LONG).show();
                            BDbHelper.updateWine(wine);

                            break;
                        case 2:

                               boolean existe = BDbHelper.Trouve(wine) ;



                            if (existe  == true){
                                Toast.makeText(WineActivity.this, "Le vin '" + wine.getTitle() + " existe  déja dans la base de données", Toast.LENGTH_LONG).show();
                            } else {
                                BDbHelper.addWine(wine);
                                Toast.makeText(WineActivity.this, "Le vin '" + wine.getTitle() + "' à été ajouter", Toast.LENGTH_LONG).show();
                                break;
                            }}

                    finish();
            }
        }
    });
}

}






