package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import  static com.example.tp2.WineDbHelper.cursorToWine;

public class MainActivity extends AppCompatActivity {

    private SimpleCursorAdapter SCAdapter;
    private WineDbHelper BDbHelper;
    final String[] colFrom = new String[]{"ROWID AS _id", WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION,
            WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView winelist = findViewById(R.id.wineList);

        BDbHelper = new WineDbHelper(this);
        BDbHelper.populate();

        SCAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                BDbHelper.fetchAllWines(),
                new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        winelist.setAdapter(SCAdapter);

        winelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", cursorToWine((Cursor) parent.getItemAtPosition(position)));
                intent.putExtra("request", 1);
                startActivityForResult(intent, 1);
            }
        });
        final FloatingActionButton addActionButton = findViewById(R.id.addWine_btn);
        addActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine newWine = new Wine(null, null, null, null, null);
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", newWine);
                intent.putExtra("request", 2);
                startActivityForResult(intent, 2);
            }
        });

        winelist.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                AdapterView.AdapterContextMenuInfo del = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(0, 0, 0, "Supprimer");
            }
        });

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapter = findViewById(R.id.wineList);
        Cursor target = (Cursor) adapter.getItemAtPosition(info.position);
        WineDbHelper vin = new WineDbHelper(MainActivity.this);
        vin.deleteWine(target);
        Intent delIntent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(delIntent);
        Toast.makeText(MainActivity.this, "Le vin '" + cursorToWine(target).getTitle() + "' à été bien supprimer", Toast.LENGTH_LONG).show();
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        SCAdapter.changeCursor(BDbHelper.fetchAllWines());
        SCAdapter.notifyDataSetChanged();
    }




    //to resume the activity
    @Override
    protected void onResume() {
        super.onResume();
    }

}